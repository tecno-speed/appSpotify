var colMusicas = [];
var musicaAtual;

function procurar() {
    loadingElement('btnProcurar', 'Buscando...');

    var palavraChave = document.getElementById('palavraChave').value;

    var url = "https://api.spotify.com/v1/search?q=" + palavraChave + "&type=track";

    MobileUI.ajax
        .get(url)
        .set('Authorization', 'Bearer BQCctNQ5qTspjHCYH2kmFrlrRRa8lLmlHz6hvMhiTeI-bId5eCin3KZ2J4Yr7Ryfw2wjkA7fmVsxuRh9BZFo_T88T54ca3h1gYy8772rEuzVW3NU5rI-V_gJJ1Kg11NZSesrkKtLxw-SIk62EQ')
        .end(onRetorno)
}

function onRetorno(error, obj) {
    closeLoading('btnProcurar');

    if (error) {
        alert('Ocorreu um erro')
        return;
    }

    var tracks = obj['body']['tracks'].items;

    for (var i = 0; i < tracks.length; i++) {
        var mp3 = {
            'nome_musica': tracks[i].name,
            'nome_artista': tracks[i].artists[0].name,
            'foto': tracks[i].album.images[0].url,
            'id': tracks[i].id,
            'url': tracks[i].external_urls.spotify,
            'preview': tracks[i].preview_url
        }

        colMusicas.push(mp3);
    }
}

function abrir(url) {
    window.open(url);
}

function play(preview) {
    if (!preview) {
        alert('Não existe preview desta musica')
        return;
    }

    if(musicaAtual){
        musicaAtual.pause();
    }

    musicaAtual = new Audio(preview);
    musicaAtual.play();
}

function stop() {
    musicaAtual.pause();
}